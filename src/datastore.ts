import { PostgreStore } from '@nex/pg';
import { config } from './helper/config';
import { VoiceRecord, VoiceRecordAddress, VoiceRecordAssociate,
  VoiceRecordCase, VoiceRecordTracking } from './models';
import { OAuthVendorAccess, UserAdapter } from './modules/user';

const dbConfig = config.get('db.postgres.default.connection');
export const DataStores = new PostgreStore(dbConfig, [
  { modelClass: VoiceRecord },
  { modelClass: VoiceRecordCase },
  { modelClass: VoiceRecordAddress },
  { modelClass: VoiceRecordTracking },
  { modelClass: VoiceRecordAssociate },
  { adapterClass: UserAdapter },
  { modelClass: OAuthVendorAccess },
]);
