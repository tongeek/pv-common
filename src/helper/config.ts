import { Config } from '@nex/helper';
import * as Path from 'path';

const configDirectory = Path.join(__dirname, '../../config');
export const config = new Config(configDirectory);
