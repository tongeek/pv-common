import { ErrorManager, IErrorManagerOption } from '@nex/helper';
import { config } from './config';

const errManager = new ErrorManager([], config.get<IErrorManagerOption>('i18n'));
export { errManager };
