import { VoiceRecordAddress } from './voice-record-address';
import { VoiceRecordAssociate } from './voice-record-associate';

import { IModelAttributeMap } from '@nex/helper';
import { PostgreModel, RELATIONSHIP } from '@nex/pg-model';
import { VoiceRecordCase } from './voice-record-case';
import { VoiceRecordTracking } from './voice-record-tracking';

export interface IFileInfo {
  bucketName: string;
  fileName: string;
  url: string;
  extension: string;
}

export class VoiceRecord extends PostgreModel {

  /**
   * Table name of model
   *
   * @type {string}
   */
  public readonly tableName: string = 'voice_record';

  /**
   * Table schema
   *
   * @type {string}
   */
  public readonly tableSchema: string = 'pv';

  public id: number;
  public damageTypeCode: string;
  public locationTypeCode: string;
  public targetTypeCode: string;
  public targetName: string;

  public targetWebsite: string;

  public targetPhoneNumber: string;
  public targetFax: string;
  public targetEmail: string;
  public summary: string;
  public orderNumbers: string[];

  public contactFirstName: string;
  public contactLastName: string;
  public contactPhone: string;
  public contactEmail: string;

  public customerNumbers: string[];
  public evidenceLinks: string[];
  public attachments: IFileInfo[];
  public userId: number;
  public contactMore: boolean;

  public metadata: any;
  public changelog: any;

  public createdAt: string;
  public createdBy: number;
  public updatedAt: string;
  public updatedBy: number;
  public statusCode: string;

  public voiceRecordCases: VoiceRecordCase[];
  public voiceRecordAddress: VoiceRecordAddress[];
  public voiceRecordTrackings: VoiceRecordTracking[];
  public voiceRecordAssociates: VoiceRecordAssociate[];

  public attributes(): IModelAttributeMap {
    return {
      id: 'number',
      damageTypeCode: 'string',
      locationTypeCode: 'string',
      targetTypeCode: 'string',
      targetName: 'string',

      targetWebsite: 'string',
      targetPhoneNumber: 'string',
      targetFax: 'string',
      targetEmail: 'string',

      summary: 'string[]',
      orderNumbers: 'string[]',
      customerNumbers: 'string[]',
      evidenceLinks: 'string[]',
      attachments: 'jsonb',

      contactEmail: 'string',
      contactPhone: 'string',
      contactFirstName: 'string',
      contactLastName: 'string',
      userId: 'int',
      contactMore: 'boolean',
      metadata: 'jsonb',

      createdAt: 'string',
      createdBy: 'int',
      updatedAt: 'string',
      updatedBy: 'int',
      statusCode: 'string',
    };
  }

  public relationship(): any {
    return {
      voiceRecordCases: {
        modelClass: VoiceRecordCase,
        joinOn: { id: 'voice_record_id' },
        relType: RELATIONSHIP.ONE_MANY,
      },
      voiceRecordAddresses: {
        modelClass: VoiceRecordAddress,
        joinOn: { id: 'subject_id'},
        relType: RELATIONSHIP.ONE_MANY,
      },
      voiceRecordAssociates: {
        modelClass: VoiceRecordAssociate,
        joinOn: { id: 'voice_record_id' },
        relType: RELATIONSHIP.ONE_MANY,
      },
      voiceRecordTrackings: {
        modelClass: VoiceRecordTracking,
        joinOn: { id: 'voice_record_id'},
        relType: RELATIONSHIP.ONE_MANY,
      },
    };
  }
}
