import { IModelAttributeMap } from '@nex/helper';
import { PostgreModel } from '@nex/pg-model';

export class VoiceRecordAssociate extends PostgreModel {

  /**
   * Table name of model
   *
   * @type {string}
   */
  public readonly tableName: string = 'voice_record_associate';

  /**
   * Table schema
   *
   * @type {string}
   */
  public readonly tableSchema: string = 'pv';

  public id: number;
  public voiceRecordId: number;
  public nickname: string;
  public socialNetworkCode: string;
  public position: string;

  public createdAt: string;
  public createdBy: number;
  public updatedAt: string;
  public updatedBy: number;
  public statusCode: string;

  public attributes(): IModelAttributeMap {
    return {
      id: 'number',
      voiceRecordId: 'int64',
      nickname: 'string',
      position: 'string',
      socialNetworkCode: 'string',

      createdAt: 'string',
      createdBy: 'int',
      updatedAt: 'string',
      updatedBy: 'int',
      statusCode: 'string',
    };
  }

}
