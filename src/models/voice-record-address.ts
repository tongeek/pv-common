import { IModelAttributeMap } from '@nex/helper';
import { PostgreModel } from '@nex/pg-model';

export class VoiceRecordAddress extends PostgreModel {

  /**
   * Table name of model
   *
   * @type {string}
   */
  public readonly tableName: string = 'voice_record_address';

  /**
   * Table schema
   *
   * @type {string}
   */
  public readonly tableSchema: string = 'pv';

  public id: number;
  public address: string;
  public admin3Name: string;
  public admin3Code: string;
  public admin2Name: string;
  public admin2Code: string;

  public admin1Name: string;
  public admin1Code: string;
  public countryName: string;
  public countryCode: string;
  public userId: number;

  public typeCode: string;
  public subjectId: number;
  public subjectTypeCode: string;
  public parentId: number;
  public latitude: number;
  public longitude: number;

  public metadata: any;
  public changelog: any;

  public createdAt: string;
  public createdBy: number;
  public updatedAt: string;
  public updatedBy: number;
  public statusCode: string;
  public postalCode: string;
  public coordinates: number[];

  public modelAlias(): string {
    return 'voiceRecordAddresses';
  }

  public attributes(): IModelAttributeMap {
    return {
      id: 'int',
      address: 'string',
      admin3Name: 'string',
      admin3Code: 'string',
      admin2Name: 'string',
      admin2Code: 'string',

      admin1Name: 'string',
      admin1Code: 'string',
      postalCode: 'string',
      countryName: 'string',
      countryCode: 'string',
      userId: 'int',

      typeCode: 'string',
      subjectId: 'int',
      subjectTypeCode: 'string',
      parentId: 'int64',
      latitude: 'float',
      longitude: 'float',

      metadata: 'jsonb',
      createdAt: 'string',
      createdBy: 'int',
      updatedAt: 'string',
      updatedBy: 'int',
      statusCode: 'string',
    };
  }

  public beforeSave(isNewRecord: boolean): void {
    super.beforeSave(isNewRecord);

    if (!this.longitude && !this.latitude
      && this.coordinates && this.coordinates.length > 0) {

      this.longitude = this.coordinates[0];
      this.latitude = this.coordinates[1];
    }
  }
}
