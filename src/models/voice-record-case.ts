import { IModelAttributeMap } from '@nex/helper';
import { PostgreModel } from '@nex/pg-model';

export class VoiceRecordCase extends PostgreModel {

  /**
   * Table name of model
   *
   * @type {string}
   */
  public readonly tableName: string = 'voice_record_case';

  /**
   * Table schema
   *
   * @type {string}
   */
  public readonly tableSchema: string = 'pv';

  public id: number;
  public voiceRecordId: number;
  public caseNumber: string;
  public caseWith: string;

  public createdAt: string;
  public createdBy: number;
  public updatedAt: string;
  public updatedBy: number;
  public statusCode: string;

  public attributes(): IModelAttributeMap {
    return {
      id: 'number',
      voiceRecordId: 'int64',
      caseNumber: 'string',
      caseWith: 'string',

      createdAt: 'string',
      createdBy: 'int',
      updatedAt: 'string',
      updatedBy: 'int',
      statusCode: 'string',
    };
  }

}
