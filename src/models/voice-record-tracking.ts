import { IModelAttributeMap } from '@nex/helper';
import { PostgreModel } from '@nex/pg-model';

export class VoiceRecordTracking extends PostgreModel {

  /**
   * Table name of model
   *
   * @type {string}
   */
  public readonly tableName: string = 'voice_record_tracking';

  /**
   * Table schema
   *
   * @type {string}
   */
  public readonly tableSchema: string = 'pv';

  public id: number;
  public voiceRecordId: number;
  public trackingNumber: string;
  public addressId: number;

  public createdAt: string;
  public createdBy: number;
  public updatedAt: string;
  public updatedBy: number;
  public statusCode: string;

  public attributes(): IModelAttributeMap {
    return {
      id: 'number',
      voiceRecordId: 'int64',
      trackingNumber: 'string',
      addressId: 'int64',

      createdAt: 'string',
      createdBy: 'int',
      updatedAt: 'string',
      updatedBy: 'int',
      statusCode: 'string',
    };
  }

}
