import { IRequest, NotFoundError, STATUS } from '@nex/helper';
import * as bcrypt from 'bcrypt';
import * as Bluebird from 'bluebird';
import { User, USER_TYPE } from '../models/user';

export function register(request: IRequest, user: any): Bluebird<User> {

  const userStore = request.dataStore.getStore<User>('User');
  const form = typeof user.IDKey === 'function' ? user : new User(user);
  form.typeCode = USER_TYPE.MEMBER;

  form.statusCode = STATUS.ACTIVE;
  // insert new user
  const saltRounds = request.config.getDefault('security.saltRounds', 8);

  return (bcrypt.hash(user.password, saltRounds) as any as Bluebird<string>).then((hash) => {
    user.password = hash;
    return userStore.insertOne(form);
  });
}

export function getUserOrRegister(request, input: any): Bluebird<User> {
  const userStore = request.dataStore.getStore('User');
  const form = new User(input);

  const keywords = form.phoneNumber || form.email;
  return userStore.findOne({ exactKeywords: keywords}).then((user) => {
    return Bluebird.resolve(user);
  }).catch(NotFoundError, () => {
    return register(request, form);
  });
}
