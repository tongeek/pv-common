import { IRequest, mapSnakeCaseAttributes, NotFoundError, STATUS } from '@nex/helper';
import * as bcrypt from 'bcrypt';
import * as Bluebird from 'bluebird';
import { OAUTH_PROVIDER, OAuthVendorAccess } from '../models/oauth-vendor-access';
import superagent = require('superagent-bluebird-promise');
import { User, USER_TYPE } from '../models/user';

export function login(request, input): Bluebird<User> {
  const userStore = request.dataStore.getStore('User');

  return userStore.findOne({ email: input.email })
    .then((user) => {
      return bcrypt.compare(input.password, user.password).then((res) => {
        if (!res) {
          return Bluebird.reject(request.errorManager.unprocessableEntityError({ code: '114'})) ;
        }

        return Bluebird.resolve(user);
      });
    });
}

export interface IFacebookGraphConfig {
  baseUrl: string;
  clientId: string;
  clientSecret: string;
}

export function authenticateFacebook(request: IRequest, payload: any): Promise<any> {
  const code = payload.code;
  const fbConfig = request.config.get<IFacebookGraphConfig>('vendors.facebook');
  const url = `${fbConfig.baseUrl}/oauth/access_token?client_id=${fbConfig.clientId}`
    + `&client_secret=${fbConfig.clientSecret}&code=${code}&redirect_uri=${payload.redirectUri}`;
  // request facebook for token and user id
  return superagent.get(url).accept('json').then((response) => {
    const tokenData = response.body;

    // get profile data
    const profileUrl = `${fbConfig.baseUrl}/me?fields=id,first_name,last_name,name,middle_name,email,birthday,gender,locale,picture,timezone
      &access_token=${tokenData.access_token}`;
    return superagent.get(profileUrl).accept('json').then((profileResponse) => {
      // query in oauth access vendors
      const oauthVendorAccessStore = request.dataStore.getStore('OAuthVendorAccess');
      const profile = profileResponse.body;

      return oauthVendorAccessStore.findOne({
        identifier: profile.id, providerCode: OAUTH_PROVIDER.FACEBOOK,
      }, { include: ['profile'] }).then((vendorAccess: OAuthVendorAccess) => {

        // if existed, return user
        if (vendorAccess.profile.id) {
          return Bluebird.resolve(vendorAccess.profile);
        }

        throw request.errorManager.notFoundError();
      }).catch(NotFoundError, () => {
        // else insert into database
        const userStore = request.dataStore.getStore('User');
        const form = new User();
        mapSnakeCaseAttributes(form, profile);
        form.id = null;
        form.displayName = profile.name;
        form.metadata = { gmt: profile.timazone };
        form.typeCode = USER_TYPE.MEMBER;
        form.statusCode = STATUS.ACTIVE;
        form.avatar = {
          original: { url: profile.picture.data.url },
        };

        return userStore.insertOne(form).then((user: User) => {
          // insert into oauth vendor access
          const ovcForm = new OAuthVendorAccess({
            identifier: profile.id,
            providerCode: OAUTH_PROVIDER.FACEBOOK,
            statusCode: STATUS.ACTIVE,
            userId: user.id,
            createdBy: user.id,
            updatedBy: user.id,
          });

          return oauthVendorAccessStore.insertOne(ovcForm).then(() => {
            return Bluebird.resolve(user);
          });
        });
      });

    });

  }).catch((err) => {
    if (err.status === 400) {
      const errors = request.errorManager.unprocessableEntityError({
        title: err.body.error.message, detail: err.body.error.message });
      return Bluebird.reject(errors);
    }
    return Bluebird.reject(err);
  });

}
