import { IModelAttributeMap } from '@nex/helper';
import { PostgreModel } from '@nex/pg-model';

export class User extends PostgreModel {

  /**
   * Table name of model
   *
   * @type {string}
   */
  public readonly tableName: string = 'user';

  /**
   * Table schema
   *
   * @type {string}
   */
  public readonly tableSchema: string = 'pv';

  public id: number;
  public email: string;
  public username: string;
  public password: string;
  public firstName: string;

  public lastName: string;
  public middleName: string;
  public displayName: string;
  public phoneNumber: string;
  public phoneCode: string;

  public dateOfBirth: string;
  public gender: string;
  public avatar: any;
  public passwordReset: string;
  public passcode: string;

  public countryCode: string;
  public isVerified: boolean;
  public roleIds: string[];
  public permissions: any;
  public typeCode: UserType;

  public metadata: any;
  public settings: any;
  public createdAt: string;
  public createdBy: number;
  public updatedAt: string;
  public updatedBy: number;
  public statusCode: string;

  public attributes(): IModelAttributeMap {
    return {
      id: 'number',
      email: 'string',
      username: 'string',
      password: 'string',
      firstName: 'string',

      middleName: 'string',
      lastName: 'string',
      displayName: 'string',
      phoneNumber: 'string',
      phoneCode: 'string',

      dateOfBirth: 'date',
      gender: { default: 'male' },
      avatar: 'jsonb',
      passwordReset: 'string',
      passcode: 'string',
      countryCode: 'string',
      typeCode: {
        default: USER_TYPE.GUEST,
      },

      createdAt: 'string',
      createdBy: 'int',
      updatedAt: 'string',
      updatedBy: 'int',
      statusCode: 'string',
    };
  }

}

export type UserType = 'guest' | 'member';

export const USER_TYPE: { [key: string]: UserType} = {
  GUEST: 'guest',
  MEMBER: 'member',
};
