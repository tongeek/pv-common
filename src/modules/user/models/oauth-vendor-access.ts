import { IModelAttributeMap } from '@nex/helper';
import { PostgreModel, RELATIONSHIP } from '@nex/pg-model';
import { User } from './user';

export const OAUTH_PROVIDER = {
  FACEBOOK: 'fb',
};

export class OAuthVendorAccess extends PostgreModel {

  /**
   * Table name of model
   *
   * @type {string}
   */
  public readonly tableName: string = 'oauth_vendor_access';

  /**
   * Table schema
   *
   * @type {string}
   */
  public readonly tableSchema: string = 'pv';

  public id: number;
  public identifier: string;
  public providerCode: string;
  public userId: number;

  public metadata: any;
  public createdAt: string;
  public createdBy: number;
  public updatedAt: string;
  public updatedBy: number;
  public statusCode: string;
  public profile: User;

  public attributes(): IModelAttributeMap {
    return {
      id: 'number',
      identifier: 'string',
      providerCode: 'string',
      userId: 'number',
      metadata: 'string',

      createdAt: 'string',
      createdBy: 'int',
      updatedAt: 'string',
      updatedBy: 'int',
      statusCode: 'string',
    };
  }

  public relationship(): any {
    return {
      profile: {
        modelClass: User,
        joinOn: { user_id: 'id' },
        relType: RELATIONSHIP.ONE_ONE,
      },
    };
  }
}
