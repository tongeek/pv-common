import { PostgreAdapter } from '@nex/pg';
import { User } from '../models/user';

export class UserAdapter extends PostgreAdapter<User> {

  get modelClass() {
    return User;
  }

  /**
   * Filter params into query condition
   *
   * @param  {Object} params Query param data
   *
   * @return {Object}        Filtered query result
   */
  protected filterParams(params: any, options?: any) {
    const condition = super.filterParams(params, options);
    if (params.exactKeywords) {
      condition.where.push(`t0.phone_number = $${condition.paramCount++} OR t0.email = $${condition.paramCount++}`);
      condition.args.push(params.exactKeywords, params.exactKeywords);
    }

    return condition;
  }
}
